/**
* This sketch is drawing random ellipses
* on the screen.
*/

void setup(){
   size(600,800);
}


void draw(){
  // draws ellipse
  drawRandomEllipse();
  drawRandomEllipse();
  // hello  
}


void drawRandomEllipse(){
  fill(#FF0000);
  float x = random(width);
  float y = random(height);
  float ellipseWidth = random(10,20);
  float ellipseHeight = random(30, 40);
  ellipse(x, y, ellipseWidth, ellipseHeight);
   
}
